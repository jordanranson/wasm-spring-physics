const { readFileSync } = require('fs')

const run = async () => {
  const buffer = readFileSync('./index.wasm')
  const module = await WebAssembly.compile(buffer)
  const instance = await WebAssembly.instantiate(module)

  const { update } = instance.exports

  const state = {
    x: 0,
    velx: 0,
    speed: 0.015,
    resistance: 0.875
  }

  setInterval(() => {
    state.velx = update(state.velx, state.x, 10, state.speed, state.resistance)
    state.x += state.velx

    console.clear()
    console.log('x:', state.x)
  }, 1000/30)
}

run()
