const wabt = require('wabt')()
const path = require('path')

const {
  readFileSync,
  writeFileSync } = require('fs')

const inputWat = './src/index.wat'
const outputWasm = './index.wasm'

const wasmModule = wabt.parseWat(inputWat, readFileSync(inputWat, 'utf8'))
const { buffer } = wasmModule.toBinary({})

writeFileSync(outputWasm, new Buffer(buffer))

console.log('Done!')

process.exit()
