(module
  (func $update (param $velocity f64) (param $value f64) (param $target f64) (param $speed f64) (param $resistance f64) (result f64)
    get_local $target
    get_local $value
    f64.sub
    get_local $speed
    f64.mul
    get_local $velocity
    f64.add
    get_local $resistance
    f64.mul
  )
  (export "update" (func $update))
)
